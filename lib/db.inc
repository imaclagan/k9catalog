<?php



//include_once 'db.class.php';




function was_do_query($query)
{

	global $db;

	// dd($db);

	global $db_debug;

	$ret = false;

	// set $db->debug for debug info

	if ($db_debug) {

		$db->debug = true;
	} else {

		$db->debug = false;
	}



	if (preg_match("/^[\s]*select[\s]+/im", $query)) {

		// return assoc array		 

		$ret = $db->fetchrows($query);
	}



	if (preg_match("/^[ ]*insert /im", $query)) {

		$ret = $db->query($query);
	}



	if (preg_match("/^[ ]*delete /im", $query)) {

		$ret = $db->Execute($query);
	}

	if (preg_match("/^[ ]*update /im", $query)) {

		$ret = $db->Execute($query);
	}

	return $ret;
}



function was_query_to_array($res, $key = 'id')
{

	$ret = array();

	if (is_array($res) && count($res) > 0) {

		foreach ($res as $row) {

			$ret[$row[$key]] = $row;
		}
	}

	return $ret;
}
