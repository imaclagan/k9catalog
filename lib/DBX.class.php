<?php
class DBX
{

    private static $lastQuery;
    private static $error;
    private static $dbh;
    private static $instance = null;

    public static function instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new DBX();
        }
        return self::$instance;
    }

    private final  function __construct()
    {
        if (!self::$dbh = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE)) {
            die("Could not connect : " . mysqli_connect_error());
        }
    }

    public static function getConn()
    {
        return self::$dbh;
    }

    private function __clone()
    { // private to stop clone
    }

    public static function dbh()
    {
        return self::$dbh;
    }

    public static function qstr($data)
    {
        return self::quote($data);
    }

    public static function GetArray($sql)
    {
        return self::fetchrows($sql);
    }

    public static function Insert_ID()
    {
        return mysqli_insert_id(self::$dbh);
    }

    public static function quote($data, $type = 'varchar')
    {
        return in_array($type, array(
            'varchar',
            'char',
            'date',
            'datetime',
            'blob',
            'mediumblob',
            'text',
            'mediumtext',
        )) ? "'" . mysqli_real_escape_string(self::$dbh, $data) . "'" : $data;
    }

    public static function execute($sql)
    {
        self::$error = false;
        self::$lastQuery = $sql;

        $result = mysqli_query(self::$dbh, $sql);

        if (!$result) {
            self::$error = 'Database error: ' . mysqli_error(self::$dbh);
        }
        return $result;
    }
    /**
     *  @desc make a database query
     *  returns insert_id if INSERT , affected_rows if UPDATE , result ID if SELECT
     *
     */
    public static function query($sql)
    {
        self::$error = false;
        self::$lastQuery = $sql;

        $sql = trim($sql);

        $result = mysqli_query(self::$dbh, $sql);
        if ($result) {
            // no query errors
            if (preg_match("/^\binsert\b\s+/i", $sql)) {
                return mysqli_insert_id(self::$dbh);
            } elseif (preg_match("/^\b(update|delete|replace)\b\s+/i", $sql)) {
                return mysqli_affected_rows(self::$dbh);
            } else {
                // if query returns data
                if (mysqli_num_rows($result)) {
                    return $result;
                } else {
                    return false;
                }
            }
        } else {
            // query failed

            self::$error = 'Database error: ' . mysqli_error(self::$dbh);
            //pr(self::$error);exit;
            return -1; //cant use false as this is the same result as no rows returned from query
        }
    }

    public static function ajaxQuery($sql)
    {
        if (!empty($sql)) {
            if ($query = self::query($sql)) {
                while ($row = mysqli_fetch_assoc($query)) {
                    $result[] = $row;
                }
                return array(
                    'replyCode' => '200',
                    'replyText' => 'Ok',
                    'data' => $result,
                );
            } else {
                return array(
                    'replyCode' => '500',
                    'replyText' => self::$error . " SQL=" . $sql,
                    'data' => array(),
                );
            }
        }
    }

    /**
     * @desc return a multirow result set as assoc array
     */
    public static function fetchRows($sql, $key = '')
    {
        if (!empty($sql)) {
            $result = array();
            $query = self::query($sql);

            if (!self::$error) {
                if ($query) {
                    while ($row = mysqli_fetch_assoc($query)) {
                        if (empty($key) || !isset($row[$key])) {
                            $result[] = $row;
                        } else {

                            $result[$row[$key]] = $row;
                        }
                    }
                }

                return $result;
            } else {
                flashError(self::$error);
                flashError(self::$lastQuery);
                return false;
            }
        }
    }
    /**
     * @desc return a single result set as assoc array
     */
    public static function fetchRow($sql)
    {
        if (!empty($sql)) {
            $result = array();
            $query = self::query($sql);

            if ($query) {
                $result = mysqli_fetch_assoc($query);
            }
            return $result;
        }

        return -1;
    }

    public static function fetchRecordById($table, $id)
    {
        $sql = "select * from " . $table . " where id =" . $id;
        return self::fetchRow($sql);
    }

    public static function updateRecord($table, $data, $where = '')
    {
        self::query(update_string($table, $data, $where));
    }

    public static function insertRecord($table, $data, $key = 'id')
    {
        unset($data[$key]);
        $sql = insert_string($table, $data);
        $insertId = self::query($sql);
        return $insertId;
    }

    public static function deleteRecord($table, $id)
    {
        if ($id) {
            $sql = "delete from " . $table . " where id=" . $id . " limit 1";
            self::query($sql);
        }
    }
    /**
     * @desc used to execute a srting with multiple sql statements
     *  useful for modules that read their setup sql from a file
     */
    public static function multiQuery($multipleQuerys)
    {
        $result = true;
        $querys = split(";", $multipleQuerys);

        if (is_array($querys) && count($querys)) {
            $sql = '';

            foreach ($querys as $query) {
                $sql .= rtrim($query);

                if (!empty($sql)) {
                    if (!self::execute($sql)) {
                        flashError(self::$error . '<br />QUERY=' . self::$lastQuery);
                        $result = false;
                    }
                    $sql = "";
                }
            }
        }
        return $result;
    }

    // end of class
}
