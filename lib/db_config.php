<?php

use Illuminate\Database\Capsule\Manager as Capsule;


if (APP_MODE == 'development') {

    // use mysql for laradock,use localhost for laragon
    $dbhost = "mysql"; 
    define('DB_HOST',  "db");

    $dbname = "k9";
    define('DB_DATABASE',  "db");

    $dbuname = "default";
    define('DB_USER',  "db");

    $dbpass = "secret";
    define('DB_PASSWORD',  "db");


} else {

    // it is the live environment

    $dbhost = "127.0.0.1";
    define('DB_HOST',  "127.0.0.1");

    $dbname = "k9homesc_db";
    define('DB_DATABASE',  "k9homesc_db");

    $dbuname = "k9homesc_dbuser";
    define('DB_USER',  "k9homesc_dbuser");

    $dbpass = "dazFQB23JA72";
    define('DB_PASSWORD',  "dazFQB23JA72");

}

$capsule = new Capsule;

$capsule->addConnection([
    "driver" => "mysql",
    "host" => DB_HOST,
    "database" => DB_DATABASE,
    "username" => DB_USER,
    "password" => DB_PASSWORD
],"default"); // Make Capsule available for connecting to db


$capsule->setAsGlobal();
$capsule->bootEloquent();

