<?php

$debug = true;
if($debug){
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

require "../vendor/autoload.php";
// Initial setup //
// include_once('../../config.php');
// include_once '../lib/db_config.php';
// include_once '../lib/db.inc';
// include_once '../lib/common.inc';
// include_once '../lib/state.class.php';
// include_once '../lib/session.php';
error_reporting(E_ERROR | E_WARNING | E_PARSE);
// Collect Request Vars
$req = http_request(); // Populate global var $req from $_GET and $_POST

$db = DB::getInstance();

//dd($db);


$S = getAppState($db);

if(!$S->isInternalUser()){
    die('Bad Request ...');
}
// echo pr($S);
// echo dumper ($req);
// echo dumper($_COOKIE);
// echo "USER=$user <br>\n";

/*
 *******************************************************************************************
 *                                                                                                                                                                                    *
 * Main Switch - selects the data 'module' to be loaded and then the view to be used       *
 *                                                                                                                                                                                    *
 *                                                                                                                                                                                    *
 *******************************************************************************************
 */
$template = "templates/admin_main.inc"; // default - modules can change if need be
//echo dumper($req);
$m = $req['m']; // module to use
$a = $req['a']; // action request
$v = $req['v']; // next view requested
//Actions
switch ($m) {
    case "product":
        $module = "product";
        break;
    case "stock":
        $module = "stock";
        break;
    default:
        $module = "default";
}
$file = "modules/" . $module . ".php";
if (file_exists($file)) {
    include $file;
} else {
    $error_msg .= "<p>Module '" . "modules/" . $module . ".php" . " is not available</p>\n";
}
// Views
switch ($m) {
    
    case "product":
        if ($req['a'] == 'edit') {
            $_view = "edit_product";
        } elseif ($req['a'] == 'specialprices') {
            $_view = "edit_specialprices";
        } elseif ($req['a'] == 'stockadjust') {
            $_view = "stockadjust";
        } else {
            $_view = "edit_product";
        }
        break;
    case "stock":
        $_view = "low_stock";
        break;
    default:
        $_view = "default";
}
include_once $template;
