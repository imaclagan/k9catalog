<?php include("templates/functions.php"); ?>

<!DOCTYPE html>
<html lang="en">

	<head>
		<?php include("templates/header.inc"); ?>
	</head>


	<body>

		<div id="pagecontainer">
			
			<?php include("templates/topbar.inc"); ?>

			<?php include("headbar.php"); ?>

			

			<?php include("messages.php"); ?>

			<?php include("leftbar.php"); ?>

			<div id="content"> <!-- start of content cell-->

			<?php include("insertview.php"); ?>

			<?php include("templates/footer.inc"); ?>


		</div><!-- end page container -->

	</body>

</html>