<?php

$files =[
    'index.php',
    'lib/db_config.php',
    'lib/DB.class.php',
    'lib/common.inc',
    'lib/State.class.php',
    'lib/session.php'
];

$includes = findIncludes($files,[]);
dd($includes);


function findIncludes($files, $includes)
{
    

    foreach($files as $file){
        $found = scanFileForIncludes($file);
        if(count($found) > 0){
            $includes[$file]=$found;
            $includes = findIncludes($found,$includes);
            
        }
        
    }

    return $includes;
}

function scanFileForIncludes($file)
{
    $content = file_get_contents($file);
    // regex that extracts includes
    $pattern = '~(include)(\s)*[\"\']([^\"\']+)"~im';
    $m = preg_match_all($pattern,$content,$matches);

    return $matches[3];
    
}

function dd($a)
{
    echo '<pre>';
    print_r($a);
    echo '</pre>';
    die('dd done');
}
