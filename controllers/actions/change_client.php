<?php
    
    save_basket(); // save the current client basket to db
    $S->clearBasket(); // Clear state basket
    unset($S->client); // Clear the working client id
    unset($S->order_contact); // Clear any Order contact
    
    $S->nextview = isset($req['v']) ? $req['v'] : "select_client";