<?php

/**
 * Count the stock the client has in store
 *
 */


$clientId = $S->getClientId();
$sortBy = isSet($_GET['sortby']) ? $_GET['sortby'] : '';

$months = (isset($_GET['m']) && (int) $_GET['m'] > 0 && (int) $_GET['m'] < 25) ? (int) $_GET['m'] : 3;

$repId = $S->id;


if ($_POST && $clientId && $repId) {
    
    // save data to clientstock table
    $sql = 'delete from clientstock where DATE(`datetime`)=DATE(NOW()) and client_id=' . $clientId
        . ' and user_id=' . $repId;
    $db->query($sql);

    foreach ($_POST['instock'] as $productCode => $instore_qty) {

        $qty = (int) $instore_qty;
        $suggestedOrderQty = (int) $_POST['order'][$productCode];
        if (strlen($instore_qty)) {
            $sql =
                'INSERT into clientstock (client_id,product_code,stock_count,suggested_order_qty,datetime,user_id) VALUES ('
                . $clientId . ',"' . $productCode . '",' . $qty . ',' . $suggestedOrderQty . ',"'
                . date('Y-m-d H:i') . '",' . $repId . ')';
            //$this->pr( $sql,1);
            $db->query($sql);
        }

            // Update basket with suggested order qty
            //NOTE: Rep can ONLY update basket qty for items they have entered
            // a valid Instore qty
            if ($suggestedOrderQty > 0) {
                // Need to check if product has enough  instock to do this
                $S->basket[$productCode] = $suggestedOrderQty;
            } else {
                unset($S->basket[$productCode]);
            }
        
    }
}

if (!empty($sortBy)) {
    $orderBy = " order by product_code asc";
} else {
    $orderBy = " order by tqty desc";
}

if ($clientId) {
    $sql =
        "select
            orders.order_id,
            items.product_code,
            products.description as description,
            products.size,
            products.color_name,
            products.can_backorder,
            products.qty_instock,
            products.notify_when_instock,
            type.name as type,
            sum(qty)as tqty,
            orders.client_id
            from
            system_order_items as items
            join system_orders as orders on items.order_id = orders.order_id
            join products on items.product_code = products.product_code
            join `type` on products.typeid = type.typeid
            where orders.client_id =$clientId
            and DATE_SUB(NOW(),INTERVAL " . $months . " MONTH) < orders.modified
            and products.status = 'active'
            group by orders.order_id,items.product_code ";
    $sql .= $orderBy;
    $items = $db->fetchRows($sql);


    if (is_array($items) && count($items)) {
        foreach ($items as $k => $item) {
            // check if BOM
            $bom_items = $db->fetchRows("select * from boms where parent_product_code='" . $item['product_code'] . "'");
            //                   pr($item['product_code']);
            if (count($bom_items)) {
                // item is a BOM
                $max_available = isBomAvailable($bom_items);

                if (!$max_available) {
                    unset($items[$k]); //remove the item
                } else {
                    $items[$k]['qty_instock'] = $max_available;
                }
            }
        }
    }
    // sift through items and if Bom check availability

    foreach ($items as $item) {

        if (isset($newItems[$item['product_code']])) {
            $item['tqty'] += $newItems[$item['product_code']]['tqty'];
        }
        $newItems[$item['product_code']] = $item;
    }


    // sort items by product code or tqty
    // note tqty for bom_items maybe zero given client order BOM
    if (!empty($sortBy)) {
        ksort($newItems);
    } else {
        // do nothing
    }

    setViewData('result', $newItems);

    // get clientstock data - stock count
    $sql = " select * from clientstock where client_id=$clientId and DATE(`datetime`)=DATE(NOW())";
    $clientstock = $db->fetchRows($sql, 'product_code');
    $basket = $S->basket;

    // merge basket and clientstock // basket wins
    if (is_array($basket) && count($basket)) {
        foreach ($basket as $productCode => $qty) {
            $clientstock[$productCode]['product_code'] = $productCode;
            $clientstock[$productCode]['suggested_order_qty'] = $qty;
        }
    }

    setViewData('clientstock', $clientstock);

    setViewData(
        'client',
        $db->fetchRow("select client_id,name from clients where client_id=" . $clientId)
    );

    setViewData('last_orders', clientsLastOrders($clientId, $months));
}


function clientsLastOrders($clientId, $months)
{
    global $S, $db;

    $months = 3;

    // Get the last order
    $sql = "select * from system_orders where client_id=" . $clientId . " 
        and DATE_SUB(NOW(),INTERVAL " . $months . " MONTH) < system_orders.modified order by client_id desc limit 3";

    $lastOrders = $db->fetchRows($sql);

    $lastOrdersData = [];

    if ($lastOrders) {
        foreach ($lastOrders as $lastOrder) {
            $sql =
                "select
                orders.order_id,
                items.product_code,
                items.price,
                items.qty,
                products.description as description,
                products.size,
                products.color_name,

                type.name as typename
                
                from
                system_order_items as items
                join system_orders as orders on items.order_id = orders.order_id
                join products on items.product_code = products.product_code
                join `type` on products.typeid = type.typeid
                where orders.order_id = '" . $lastOrder['order_id'] . "'";


            $lastOrdersData[$lastOrder['order_id']]['order'] = $lastOrder;
            $lastOrdersData[$lastOrder['order_id']]['items'] = $db->fetchRows($sql);
        }
    }


    return $lastOrdersData;
}

function isBomAvailable($bom_items)
    {
        global $db;
        $numbons = array();

        //pr($bom_items);
        if (is_array($bom_items) && count($bom_items)) {
            // it is a bom
            foreach ($bom_items as $item) {
                $res2 = $db->fetchRow('select qty_instock from products where product_code="'
                    . $item['item_product_code'] . '" and `status` != "inactive" ');

                // echo dumper($res2);
                if ($res2) {
                    if ($item['item_qty'] > 0) {
                        if ($res2['qty_instock'] != 0) {
                            $numboms[] = (int) ($res2['qty_instock'] / $item['item_qty']);
                        } else {
                            $numboms[] = 0;
                        }
                        //echo dumper($numboms);

                    }
                }
            }

            return min($numboms);
        }
    }
